package com.theworld.help.cbtandroid;

import static android.app.UiModeManager.MODE_NIGHT_NO;
import static android.app.UiModeManager.MODE_NIGHT_YES;
import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;

import android.app.AlertDialog;
import android.app.UiModeManager;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.Button;
import android.content.ClipboardManager;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;


public class SettingsActivity extends AppCompatActivity {
    private Button help;
    private ImageView back;
    private Button export_button;
    private Button import_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        back = (ImageView) findViewById(R.id.back);
        //Exit
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exit();
            }
        });

        //Help button
        help = findViewById(R.id.help);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://copoer.gitlab.io/CBTAndroid/"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        //Help button
        export_button = findViewById(R.id.export_button);
        export_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                export_data();
            }
        });

        //Import button
        import_button = findViewById(R.id.import_button);
        import_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warn_before_import();
            }
        });
    }



    private void warn_before_import() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Import Warning");
        alertDialog.setMessage("Importing entries will over write all existing entries");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Continue",
            new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                import_data();
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void import_data() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        String pasteData = "";
        // If it does contain data, decide if you can handle the data.
        if (!(clipboard.hasPrimaryClip())) {
            import_failed();
        } else if (!(clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN))) {
            import_failed();
            // since the clipboard has data but it is not plain text
        } else {
            //since the clipboard contains plain text.
            ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);

            // Gets the clipboard as text.
            pasteData = item.getText().toString();
        };

        if (pasteData != "") {
            ArrayList<Entry> collection = null;
            try {
                collection = new Gson().fromJson(pasteData, new TypeToken<ArrayList<Entry>>() {
                }.getType());
            } catch (Exception e) {
                System.out.println("Error " + e.getMessage());
                import_failed();
            }
            if (collection == null || collection.size() < 1) {
                import_failed();
            } else {
                Files file = new Files(this);
                file.write(collection);
            }
        }
    }

    private void import_failed() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Import Failed");
        alertDialog.setMessage("The data import failed. Your clipboard must contain a JSON array of journal entries that were previously exported.");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void export_data() {
        Files file = new Files(this);
        ArrayList<Entry> collection = file.read();
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        String export_data = new Gson().toJson(collection);
        ClipData clip = ClipData.newPlainText("Export", export_data);
        clipboard.setPrimaryClip(clip);
    }
    private void exit() {
        setResult(RESULT_OK, null);
        finish();
    }
}

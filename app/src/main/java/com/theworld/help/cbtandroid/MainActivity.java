package com.theworld.help.cbtandroid;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<String> titles = new ArrayList<>();
    private ImageView settings;
    private RecyclerView recyclerView;
    private TextView welcome;
    private CustomAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        welcome = (TextView) findViewById(R.id.welcome);

        //Button to make new entry
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.new_entry);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivityForResult(new Intent(MainActivity.this, EntryActivity.class), 1);
            }
        });

        // Settings
        settings = findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(MainActivity.this, SettingsActivity.class), 1);
            }
        });

        //Recycle view generation
        recyclerView = (RecyclerView) findViewById(R.id.entry_list);
        mAdapter = new CustomAdapter(titles);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        prepareTitleData();
    }

    //Read file and get titles
    private void prepareTitleData() {
        Files file = new Files(this);
        ArrayList<Entry> collection = file.read();
        if (collection.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            welcome.setVisibility(View.VISIBLE);
        }
        for (int i = 0; i < collection.size(); i++) {
            titles.add(collection.get(i).getTitle());
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            Intent refresh = new Intent(this, MainActivity.class);
            startActivity(refresh);
            this.finish();
        }
    }
}
